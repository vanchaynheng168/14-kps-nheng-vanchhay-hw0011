import React, { Component } from 'react'
import {  ListGroup} from "react-bootstrap";

export default class ListResult extends Component {
    render() {
        return (
            <ListGroup key={this.props.items.id}>
                <ListGroup.Item >{this.props.items.value}</ListGroup.Item>
            </ListGroup>
        )
    }
}
