import React, { Component } from "react";
import Calculator from "./Calculator.png";
import {Card,Container,Col,Row,Form,Button} from "react-bootstrap";
import ListResult from "./ListResult";

export default class calculate extends Component {
  constructor(props) {
    super(props);
    this.i = 0;
    this.state = {
      input_value1: 0,
      input_value2: 0,
      operater: "+",
      items: [],
      id: 0,
      value: 0,
    };
  }
  //get value 1
  handleInputValue1Change = (event) => {
    event.preventDefault();
    this.setState({
      input_value1: event.target.value,
    });
  };
  //get value2
  handleInputValue2Change = (event) => {
    event.preventDefault();
    this.setState({
      input_value2: event.target.value,
    });
  };
  //get select value
  handleSelectChange = (event) => {
    this.setState({
      operater: event.target.value,
    });
  };
  //button calulate
  handleSubmit = (event) => {
    var total = 0;
    this.i = this.i + 1;//increase id
    const copyArray = Object.assign([], this.state.items); //declare array

    var opterate = this.state.operater;
    // check operater
    if (opterate === "+") {
      total = Number(this.state.input_value1) + Number(this.state.input_value2);
    } else if (opterate === "-") {
      total = Number(this.state.input_value1) - Number(this.state.input_value2);
    } else if (opterate === "*") {
      total = Number(this.state.input_value1) * Number(this.state.input_value2);
    } else if (opterate === "/") {
      total = Number(this.state.input_value1) / Number(this.state.input_value2);
    } else if (opterate === "%") {
      total = Number(this.state.input_value1) % Number(this.state.input_value2);
    }
    // push data to array
    copyArray.push({
      id: this.i,
      value: total,
    });
    this.setState({
      items: copyArray,
    });
    event.preventDefault();
  };
  render() {
    let displays = this.state.items.map((items, index) => (
      <ListResult items={items} />
    ));
    return (
      <Container>
        <Row>
          <Col md={5}>
            <Card className="space mt-5">
              <Form onSubmit={this.handleSubmit}>
                <img src={Calculator} alt="calculate" />
                <input
                  type="number"
                  className="form-control mb-2  "
                  value={this.state.input_value1}
                  onChange={this.handleInputValue1Change}
                />
                <input
                  type="number"
                  className="form-control mb-2"
                  name="value2"
                  value={this.state.input_value2}
                  onChange={this.handleInputValue2Change}
                />
                <Form.Group>
                  <Form.Control
                    as="select"
                    custom
                    value={this.state.operater}
                    onChange={this.handleSelectChange}
                  >
                    <option className="bg-option" value="+">
                      + Plus
                    </option>
                    <option className="bg-option" value="-">
                      - Subtract
                    </option>
                    <option className="bg-option" value="*">
                      * Multifly
                    </option>
                    <option className="bg-option" value="/">
                      / Devide
                    </option>
                    <option className="bg-option" value="%">
                      % Module
                    </option>
                  </Form.Control>
                </Form.Group>
                <Button
                  type="submit"
                  variant="primary"
                  style={{ width: "120px" }}
                  className="btn"
                >
                  Calculate
                </Button>
              </Form>
            </Card>
          </Col>
          <Col md={5} className="mt-5">
            <h2>Result History</h2>
            {displays}
          </Col>
        </Row>
      </Container>
    );
  }
}
