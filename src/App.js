import React from "react";
import "./App.css";
// import Calculate from "./components/Calculate";
// import StateProp from "./components/StateProp";
import Calculate from "./components/Calculate";
// import CalTest from "./components/CalTest";
function App() {
  return (
    <div>
      {/* <CalTest/> */}
      <Calculate/>
      {/* <StateProp/> */}
    </div>
    
  );
}
export default App;
